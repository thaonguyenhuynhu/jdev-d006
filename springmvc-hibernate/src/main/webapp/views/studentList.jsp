<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/students.js" />"></script>
	
<style type="text/css">
table tr, th, td {
	text-align: center;
}
</style>
<title><spring:message code="student.label" /></title>
</head>
<body>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="student.header"></spring:message>
		</div>
		<div class="panel-body">
			<table class="table table-striped" id="studentHome">
				<thead>
					<tr>
						<th><spring:message code="student.table.id" /></th>
						<th><spring:message code="student.table.age" /></th>
						<th><spring:message code="student.table.firstName" /></th>
						<th><spring:message code="student.table.lastName" /></th>
						<th><spring:message code="student.table.action" /></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty students}">
							<c:forEach items="${students}" var="student">
								<tr>
									<td>${student.id}</td>
									<td>${student.age}</td>
									<td>${student.firstName}</td>
									<td>${student.lastName}</td>
									<td>
										<button class="btn btn-default"
											onclick="getStudent(${student.id}, 'VIEW');">
											<spring:message code="student.btn.view" />
										</button>
										<button class="btn btn-primary"
											onclick="getStudent(${student.id}, 'EDIT');">
											<spring:message code="student.btn.edit" />
										</button>
										<button class="btn btn-danger"
											onclick="deleteStudent(${student.id}, this);">
											<spring:message code="student.btn.delete" />
										</button>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center"><spring:message
										code="student.table.empty" /></th>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<th colspan="5">
							<button onclick="location.href='addStudent'"
								class="btn btn-primary">
								<spring:message code="student.btn.add" />
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>