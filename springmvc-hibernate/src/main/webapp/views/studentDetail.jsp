<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/students.js" />"></script>
<title><spring:message code="student.detail.label"></spring:message></title>

<style type="text/css">
table tr, th, td {
	text-align: center;
}

table th {
	font-size: 20px;
}

table td {
	font-size: 15px;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("tr.rows").click(function() {
			alert("Click!");
		});
	});
</script>
<title>More Detail</title>

</head>
<body>
	<div class="panel panel-defualt">
		<div class="panel-heding h3 text-center">
			<div class="col-md-6">Student Detail</div>
			<c:if test="${mode == 'VIEW'}">
				<div class="col-md-6">Courses</div>
			</c:if>
			<c:if test="${mode == 'EDIT' || mode == 'ADD'}">
				<div class="col-md-6">List Course</div>
			</c:if>
			<br>
			<div class="panel-body">
				<form:form action="./student" class="form-horizontal" method="post"
					modelAttribute="student">
					<form:input path="id" type="hidden" />
					<div class="col-md-6">
						<div class="form-group">
							<label for="txtFName" class="control-lable col-md-4">Firstname</label>
							<div class="col-md-6">
								<form:input type="text" path="firstName" class="form-control"
									id="txtFName" placeholder="First name"
									readonly="${mode=='VIEW'}" />
							</div>
						</div>

						<div class="form-group">
							<label for="txtLName" class="control-lable col-md-4">Lastname</label>
							<div class="col-md-6">
								<form:input type="text" path="lastName" class="form-control"
									id="txtLName" placeholder="Last name"
									readonly="${mode=='VIEW'}" />
							</div>
						</div>	

						<div class="form-group">
							<label for="txtAge" class="control-lable col-md-4">Age </label>
							<div class="col-md-6">
								<form:input type="text" path="age" class="form-control"
									id="txtAge" placeholder="Age" readonly="${mode=='VIEW'}" />
							</div>
						</div>

						<div class="form-group">
							<label for="txtStreet" class="control-lable col-md-4">Street</label>
							<div class="col-md-6">
								<form:input type="text" path="address.street"
									class="form-control" id="txtStreet" placeholder="Your street"
									readonly="${mode=='VIEW'}" />
							</div>
						</div>

						<div class="form-group">
							<label for="txtDistrict" class="control-lable col-md-4">District</label>
							<div class="col-md-6">
								<form:input type="text" path="address.district"
									class="form-control" id="txtDistrict"
									placeholder="Your district" readonly="${mode=='VIEW'}" />
							</div>
						</div>

						<div class="form-group">
							<label for="txtCity" class="control-lable col-md-4">City</label>
							<div class="col-md-6">
								<form:input type="text" path="address.city" class="form-control"
									id="txtCity" placeholder="Your city" readonly="${mode=='VIEW'}" />
							</div>
						</div>

						
						<c:if test="${mode == 'ADD' || mode == 'EDIT'}">
							<div class="form-group">
								
								<div class="col-md-6">
									<table id="tbCourses" class="table table-bordered">
										
										<tbody>
											<c:if test="${mode == 'EDIT'}">
												<c:forEach items="${student.courses}" var="course">
													<tr onclick="courseClickedForEdit(this)">
														<td>${course.id}</td>
														<td>${course.courseName}</td>
													</tr>
												</c:forEach>
											</c:if>
										</tbody>
									</table>
								</div>
							</div>
						</c:if>


						<div class="btn-group" role="group" aria-label="Basic example">
							<c:if test="${mode == 'EDIT' ||  mode == 'ADD'}">
								<button type="submit" class="btn btn-primary" name="btnSave"
									id="saveBtn" onclick="getId();">Save</button>
							</c:if>

							<c:if test="${mode == 'VIEW'}">
								<button disabled="disabled" type="submit"
									class="btn btn-primary" name="btnSave" id="saveBtn"
									onclick="getId();">Save</button>
							</c:if>
							<button type="button" class="btn btn-danger"
								onclick="location.href='./students'">Cancel</button>
						</div>
					</div>

					<c:if test="${mode == 'VIEW'}">
						<div class="col-md-6">
							<table class="table table-bordered table-dark" id="tbCourses">
								<thead>
									<tr>
										<th>Course ID</th>
										<th>Course Name</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${!empty student.courses}">
										<c:forEach items="${student.courses}" var="course">
											<tr>
												<td>${course.id}</td>
												<td>${course.courseName}</td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
					</c:if>

					<c:if test="${mode == 'EDIT' || mode == 'ADD'}">
						<div class="col-md-6">
							<table class="table table-striped table-dark" id="listCourses">
								<thead>
									<tr>
										<th>Course ID</th>
										<th>Course Name</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${courses}" var="course">
										<tr onclick="courseClicked(this)">
											<td>${course.id}</td>
											<td>${course.courseName}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<p>Please, on click the row course you choose.</p>
							<br>
							<p>If you don't  want choose : click that row again.</p>
						</div>
					</c:if>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>